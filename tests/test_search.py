from pages.main_page import MainPage
from pages.article_page import ArticlePage
from pages.search_page import SearchPage
import pytest
import allure


@allure.feature("Search")
class TestSearch:
    @allure.title("Search opens specific article")
    @pytest.mark.parametrize("article", ["Selenium", "Schwarzenegger"])
    def test_search_opens_specific_article(self, wd, article):
        main_page = MainPage(wd)
        main_page.open()
        main_page.search_for(article)
        article_page = ArticlePage(wd)
        article_page.check_if_header_name_is(article)

    @allure.title("Search opens list of results")
    @pytest.mark.parametrize("query", ["Find something", "!!!!!!!!!!"])
    def test_search_opens_list(self, wd, query):
        main_page = MainPage(wd)
        main_page.open()
        main_page.search_for_keyword(query)
        search_page = SearchPage(wd)
        search_page.check_for_results()
