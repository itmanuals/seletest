from pages.login_page import LoginPage
import allure
import pytest


@allure.feature("Auth")
class TestSearch:
    @allure.title("User logs in with valid credentials")
    @pytest.mark.parametrize("creds", [{"login": "valid", "password": "valid"}])
    def test_log_in_with_valid_credentials(self, wd, creds):
        login_page = LoginPage(wd)
        login_page.open()
        login_page.set_login(creds["login"])
        login_page.set_password(creds["password"])
        login_page.click_log_in()
        login_page.check_for_anonymous_icon()

    @allure.title("Error pops up if invalid credentials used")
    @pytest.mark.parametrize("creds", [{"login": "invalid", "password": "invalid"}])
    def test_log_in_with_invalid_credentials(self, wd, creds):
        login_page = LoginPage(wd)
        login_page.open()
        login_page.set_login(creds["login"])
        login_page.set_password(creds["password"])
        login_page.click_log_in()
        login_page.check_for_error()
