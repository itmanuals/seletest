import pytest
from pytest import ExitCode
import allure
from modules.webdriver import WebDriver
import telebot


# noinspection SpellCheckingInspection
def pytest_addoption(parser):
    parser.addoption("--timeout", action="store", default="10")
    parser.addoption("--base_url", action="store", default=None)
    parser.addoption("--headless", action="store", default="false")
    parser.addoption("--token", action="store", default=None)
    parser.addoption("--chat_id", action="store", default=None)


@pytest.fixture(autouse=True)
def wd(request):
    with allure.step("Open browser"):
        base_url = request.config.getoption("base_url")
        assert base_url is not None, "Incorrect URL"
        timeout = request.config.getoption("timeout")
        assert timeout.isnumeric() and 0 < int(timeout) < 120, "Incorrect timeout"
        headless = request.config.getoption("headless")
        assert (headless == "true" or headless == "false"), "Incorrect headless parameter"
        token = request.config.getoption("token")
        chat_id = request.config.getoption("chat_id")
        if token and chat_id:
            pytest.token = token
            pytest.chat_id = chat_id
        wd = WebDriver(timeout=int(timeout), base_url=base_url, headless=(headless == "true"))
    yield wd  # pass browser to test
    if request.node.rep_call.failed:  # attach screenshot if test fails
        allure.attach(
            wd.get_screenshot_as_png(),
            name=request.function.__name__,
            attachment_type=allure.attachment_type.PNG
        )
    with allure.step("Close browser"):
        wd.quit()  # close browser on test finish (fail or pass)


# noinspection SpellCheckingInspection
@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep


# noinspection SpellCheckingInspection
def pytest_sessionfinish(session, exitstatus):
    if (exitstatus is ExitCode.TESTS_FAILED) and hasattr(pytest, "token") and hasattr(pytest, "chat_id"):
        failed_tests = []
        for item in session.items:
            if item.rep_call.failed:
                failed_tests.append(item.name)
        failed_tests_joined = "\n".join(map(lambda failed_case: F"> {failed_case}", failed_tests))
        message = \
            F"Tests failed/collected: {session.testsfailed}/{session.testscollected}\nFails:\n{failed_tests_joined}"
        bot = telebot.TeleBot(pytest.token)
        bot.send_message(pytest.chat_id, message)
