# Selenium Tests on Python
## Requirements
- Python 3.6+
- [JDK](https://www.oracle.com/ru/java/technologies/javase-downloads.html) (to run Allure)
- [Allure](https://docs.qameta.io/allure/#_installing_a_commandline)
- Google Chrome

## Setup
### Virtual environment
Install virtualenv:
```
pip install virtualenv
```
Create virtualenv:
```
virtualenv venv
```
Activate virtualenv:
```
source venv/bin/activate
```

### Dependencies
Install project dependencies:
```
pip install -r requirements.txt
```
## Run tests
```
pytest --timeout=10 --base_url=https://en.wikipedia.org -p no:cacheprovider --alluredir=allure-results --token=<YOUR_TELEGRAM_TOKEN> --chat_id=<YOUR_TELEGRAM_CHAT_IT>
```
`--token` and `--chat_id` parameters are optional

## Reporting
Generate Allure report:
```
allure generate allure-results -o allure-report
```
Open Allure report:
```
allure open allure-report
```