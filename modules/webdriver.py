from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager
import time


class WebDriver(webdriver.Remote):
    def __init__(self, timeout, base_url, headless):
        options = webdriver.ChromeOptions()
        if headless:
            options.add_argument("--headless")
            options.add_argument("--disable-gpu")
            options.add_argument("window-size=1280,720")
        dc = DesiredCapabilities.CHROME
        # noinspection PyCallByClass
        webdriver.Chrome.__init__(
            self,
            executable_path=ChromeDriverManager().install(),
            options=options,
            desired_capabilities=dc
        )
        self.set_window_size(1280, 720)
        self.timeout = timeout
        self.base_url = base_url
        self.implicitly_wait(timeout)

    def open(self, path):
        self.get(self.base_url + path)

    def wait_for_text_to_be_present(self, how, what, value):
        for i in range(self.timeout * 2):
            element = self.find_element(how, what)
            if element.text == value:
                return
            else:
                time.sleep(0.5)
        assert False, F"Text '{value}' didn't appear"
