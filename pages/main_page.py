from pages.base_page import BasePage


class MainPage(BasePage):
    def open(self):
        self.wd.open('/wiki/Main_Page')
