from pages.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time


class LoginPage(BasePage):
    INPUT_LOGIN = (By.CSS_SELECTOR, "input#wpName1")
    INPUT_PASSWORD = (By.CSS_SELECTOR, "input#wpPassword1")
    BUTTON_LOG_IN = (By.CSS_SELECTOR, "#wpLoginAttempt")
    ALERT_ERROR = (By.CSS_SELECTOR, "div.errorbox")

    def open(self):
        self.wd.open("/w/index.php?title=Special:UserLogin")

    def set_login(self, value):
        input_login = self.wd.find_element(*self.INPUT_LOGIN)
        input_login.send_keys(value)

    def set_password(self, value):
        input_password = self.wd.find_element(*self.INPUT_PASSWORD)
        input_password.send_keys(value)

    def click_log_in(self):
        button_log_in = self.wd.find_element(*self.BUTTON_LOG_IN)
        # loop located here because click right after page load doesn't always trigger auth action
        for i in range(self.wd.timeout * 2):
            button_log_in.click()
            try:
                self.wd.find_element(*self.ICON_ANONYMOUS)
                return
            except NoSuchElementException:
                pass
            try:
                self.wd.find_element(*self.ALERT_ERROR)
                return
            except NoSuchElementException:
                pass
            time.sleep(0.5)
        assert False, "No action was performed on 'Log In' button click"

    def check_for_error(self):
        self.wd.find_element(*self.ALERT_ERROR)

    def check_for_anonymous_icon(self):
        for i in range(self.wd.timeout * 2):
            try:
                self.wd.find_element(*self.ICON_ANONYMOUS)
                time.sleep(0.5)
            except NoSuchElementException:
                return
        assert False, "Anonymous icon still appears"
