from modules.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class BasePage:
    INPUT_SEARCH = (By.CSS_SELECTOR, "input#searchInput")
    ITEM_SEARCH_FIRST = (By.XPATH, "(//div[contains(@class, 'suggestions-result')][span])[1]")
    ICON_ANONYMOUS = (By.CSS_SELECTOR, "li#pt-anonuserpage")

    def __init__(self, browser: WebDriver):
        self.wd = browser

    def search_for(self, value):
        input_field = self.wd.find_element(*self.INPUT_SEARCH)
        input_field.send_keys(value)
        self.wd.wait_for_text_to_be_present(*self.ITEM_SEARCH_FIRST, value)
        input_field.send_keys(Keys.ENTER)

    def search_for_keyword(self, value):
        input_field = self.wd.find_element(*self.INPUT_SEARCH)
        input_field.send_keys(value)
        input_field.send_keys(Keys.ENTER)
