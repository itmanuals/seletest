from pages.base_page import BasePage
from selenium.webdriver.common.by import By


class ArticlePage(BasePage):
    HEADER_MAIN = (By.CSS_SELECTOR, "h1#firstHeading")

    def check_if_header_name_is(self, value):
        element_header = self.wd.find_element(*self.HEADER_MAIN)
        article_name = element_header.text
        assert article_name == value, F"Article name must be '{value}', but it's '{article_name}'"
