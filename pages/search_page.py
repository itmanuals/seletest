from pages.base_page import BasePage
from selenium.webdriver.common.by import By


class SearchPage(BasePage):
    ITEMS_SEARCH = (By.CSS_SELECTOR, "li.mw-search-result")

    def open(self):
        self.wd.open("/w/index.php?search")

    def check_for_results(self):
        results = self.wd.find_elements(*self.ITEMS_SEARCH)
        assert len(results) > 0, "No search results found"
